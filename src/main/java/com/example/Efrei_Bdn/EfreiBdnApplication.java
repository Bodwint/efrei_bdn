package com.example.Efrei_Bdn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EfreiBdnApplication {

	public static void main(String[] args) {
		SpringApplication.run(EfreiBdnApplication.class, args);
	}

}
